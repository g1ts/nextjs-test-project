export interface ICostomError extends Error {
    info?: any
    status?: number
}