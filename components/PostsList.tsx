import useSWR from "swr";
import MainLayout from "./MainLayout";
import { IPost } from "../interfaces/post";

export function PostsList() {
    // const { posts, error } = useSWR('/api/posts', fetcher)
    const { data: posts, error } = useSWR<IPost[]>('/api/posts');
    if (error) { return (<MainLayout>Failed to load</MainLayout>); }
    if (!posts) { return (<MainLayout>Loading...</MainLayout>); }

    return (
        <div>
            {posts.map(post => <section key={post.id}>
                <h2>{post.title}</h2>
                <p>{post.text}</p>
            </section>)}
        </div>);
}
