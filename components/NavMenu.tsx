import Router from 'next/router';
import Link from "next/link";

import styles from '../styles/NavMenu.module.css'

function NavMenu() {
    return (
        <nav className={styles.nav_menu}>
            <button onClick={() => { Router.push('/') }}>Home</button>
            <Link href="/posts"><a>Posts</a></Link>
            <Link href="/users"><a>Users</a></Link>
        </nav>
    )
}

export default NavMenu