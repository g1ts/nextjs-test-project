import Head from 'next/head'
import React from 'react'
import NavMenu from './NavMenu'

import styles from '../styles/MainLayout.module.css'

export default function MainLayout({ children, title = 'Test app1' }) {
    return (
        <>
            <Head>
                <title>{title} | Test app</title>
                <meta charSet='utf8' />
            </Head>
            <NavMenu />
            <main className={styles.main}>
                {children}
            </main>
        </>
    )
}
