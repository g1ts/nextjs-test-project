import MainLayout from "../components/MainLayout";

export default function Home() {
  return (<MainLayout title="Home page">
      <h1>Main page</h1>
      <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestiae repellat, veritatis, minus voluptates molestias rerum nihil deserunt similique eveniet, excepturi commodi assumenda vitae soluta eius consectetur illo voluptatibus est harum?</p>
  </MainLayout>)
}
