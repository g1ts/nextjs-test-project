import MainLayout from "../components/MainLayout"
import Link from "next/link"
import useSWR from "swr"
// import { useState, useEffect } from 'react';

export default function UsersPage() {
    // export default function UsersPage({ users: usersFromServer }) {
    // const [users, setUsers] = useState(usersFromServer)

    // useEffect(() => {
    //     const fetchUser = async () => {
    //         const res = await fetch(`http://localhost:3333/users`)
    //         const _users = await res.json()
    //         setUsers(_users)
    //     }
    //     if (!usersFromServer) {
    //         fetchUser()
    //     }
    // }, [])

    const { data: users, error } = useSWR(process.env.API_BASE_URL + '/users')

    if (error) return (<MainLayout>Failed to load</MainLayout>)
    if (!users) return (<MainLayout>Loading...</MainLayout>)


    return (
        <MainLayout>
            <h1>Users</h1>
            <div>
                {users.map(u => <div key={u.id}>
                    <Link href={`/user/[id]`} as={`/user/${u.id}`}><a>{u.name}</a></Link>
                </div>)}
            </div>
        </MainLayout>
    )
}

/*
UsersPage.getInitialProps = async ({ req }) => {
    if (!req) {
        return { users: null }
    }
    const res = await fetch('http://localhost:3333/users')
    const users = await res.json()

    if (!users) {
        return {
            notFound: true,
        }
    }

    return { users }
}
*/

// export async function getServerSideProps(ctx) {
//     const res = await fetch('http://localhost:3333/users')
//     const users = await res.json()

//     if (!users) {
//         return {
//             notFound: true,
//         }
//     }

//     return {
//         props: { users }
//     }
// }
