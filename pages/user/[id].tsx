import { useRouter } from 'next/router';
import MainLayout from '../../components/MainLayout';
import { useState, useEffect } from 'react';
import { NextPageContext } from 'next';
import { IUser } from '../../interfaces/user';

interface IUserPageProps {
    user: IUser
}


export default function UserPage({ user: userFromServer }: IUserPageProps) {
    const r = useRouter()

    const [user, setUser] = useState(userFromServer)

    useEffect(() => {
        const fetchUser = async () => {
            const res = await fetch(`${process.env.API_BASE_URL}/users/${r.query.id}`)
            const _user = await res.json()
            setUser(_user)
        }
        if (!user) {
            fetchUser()
        }
    }, [])


    if (!user) {
        return (<MainLayout>Loading...</MainLayout>)
    }

    return (<MainLayout title={`User ${user.name}`}>
        <h1>{user.name}</h1>
        <pre>{JSON.stringify(user, null, 2)}</pre>
    </MainLayout>)
}

interface UserNextPageContext extends NextPageContext {
    query: {
        id: string
    }
}

UserPage.getInitialProps = async ({ query, req }: UserNextPageContext) => {
    if (!req) {
        return { user: null }
    }

    const res = await fetch(`${process.env.API_BASE_URL}/users/${query.id}`)
    const user = await res.json()

    if (!user) {
        return {
            notFound: true,
        }
    }

    return { user }
}

// export async function getServerSideProps(ctx) {
//     console.log(ctx.query);
//     const res = await fetch(`http://localhost:3333/users/${ctx.query.id}`)
//     const user = await res.json()

//     if (!user) {
//         return {
//             notFound: true,
//         }
//     }

//     return {
//         props: { user }
//     }
// }
