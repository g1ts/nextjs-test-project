import NextNProgress from 'nextjs-progressbar'
import { SWRConfig } from 'swr'
import { ICostomError } from '../interfaces/error'
import '../styles/globals.css'

function MyApp({ Component, pageProps }) {

  return <SWRConfig
    value={{
      // refreshInterval: 3000,
      fetcher: async url => {
        const res = await fetch(url)
      
        if (!res.ok) {
          const error: ICostomError = new Error('An error occurred while fetching the data.')
          error.info = await res.json()
          error.status = res.status
          throw error
        }
      
        return res.json()
      }
    }}
  >
    <NextNProgress />
    <Component {...pageProps} />
  </SWRConfig>

}

export default MyApp
