// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import { NextApiRequest, NextApiResponse } from "next";
import { IPost } from "../../interfaces/post";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  // return res.status(500).json({ error: 'some error' });
  try {
    const posts = await getPosts()
    res.status(200).json(posts)
  } catch (error) {
    res.status(500).json({ error });
  }
  
}



export const getPosts = async (): Promise<IPost[]> => {
  // await connectToDatabase();
  // return await Posts.find();
  return [
    { id: 1, title: 'titl 1', text: 'some text 1' },
    { id: 2, title: 'titl 2', text: 'some text 2' },
    { id: 3, title: 'titl 3', text: 'some text 3' }
  ]
};