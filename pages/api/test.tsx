// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import { NextApiRequest, NextApiResponse } from "next";

interface ITestNextApiRequest extends NextApiRequest {
  query: {
    somekey?: string
  }
}

export default async function handler(req: ITestNextApiRequest, res: NextApiResponse) {
  res.statusCode = 200
  res.setHeader('Content-Type', 'application/json')
  res.end(JSON.stringify({
    someText: req.query.somekey ?? 'some default text'
  }))
}

