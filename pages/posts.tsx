import { SWRConfig } from "swr";
import MainLayout from "../components/MainLayout";
import { IPost } from "../interfaces/post";
import { getPosts } from "./api/posts";
import { PostsList } from "../components/PostsList";


interface IPostsPageProps {
    fallback: { [key: string]: IPost[] }
}

export default function PostsPage({ fallback }: IPostsPageProps) {
    return (
        <MainLayout>
            <h1>Posts</h1>
            <SWRConfig value={{ fallback }}>
                <PostsList />
            </SWRConfig>
            <style jsx>{`
                section {
                    margin: 10px;
                    background-color: #DDD;
                    padding: 10px
                }
            `}</style>
        </MainLayout>
    )
}

export async function getStaticProps() {
    // `getStaticProps` is executed on the server side.
    const posts = await getPosts()
    return {
        props: {
            fallback: {
                '/api/posts': posts
            }
        }
    }
}

